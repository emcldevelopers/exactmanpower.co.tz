<a href="{{ $advert->getURL() }}" target="_blank">
    <img class="w-100" src="{{ $advert->getImageUrl() }}" alt="{{ $advert->alt }}">
</a>
