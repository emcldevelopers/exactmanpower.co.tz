
<section class="section-padding clearfix bg-dotted {{ isset($background)? $background:'bg-default' }}">
    <div class="container">
        <h2 class="text-center text-secondary display-4 w-90 mx-auto mb-5">Bridging process through people</h2>
        <h3 class=" w-80 mx-auto mb-5 text-center">We are flexible to fit in your desired business model and so we deliver tailored services in different ways to suit your business.</h3>
        <div class="text-center">
            <a href="{{ url('contact') }}" class="btn btn-primary btn-lg rounded-pill text-white py-3 mx-4">Contact Us</a>
        </div>
    </div>
</section>